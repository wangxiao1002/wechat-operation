package com.idiot.operationbackend.config;

import com.idiot.operationbackend.support.CustomException;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.client.RestTemplateCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.time.Duration;

/**
 * RestTemplateConfig 配置 配置多个区别微服务调用和http 域名调用
 * @author wang xiao
 * @date Created in 17:31 2020/9/11
 */
@Configuration
public class RestTemplateConfig {

    @Bean("rpcRestTemplate")
    public RestTemplate buildRpc (RestTemplateBuilder builder) {
      return   builder.setConnectTimeout(Duration.ofSeconds(3))
                .setReadTimeout(Duration.ofSeconds(3))
                .defaultHeader("ContentType", MediaType.APPLICATION_JSON_VALUE)
                .errorHandler(customerErrorHandler())
                .build();
    }

    public ResponseErrorHandler customerErrorHandler() {
        return new ResponseErrorHandler() {
            @Override
            public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
                return clientHttpResponse.getStatusCode().isError();
            }

            @Override
            public void handleError(ClientHttpResponse clientHttpResponse) throws IOException {
                    throw new CustomException(500, clientHttpResponse.getStatusText());
            }
        };
    }

}
