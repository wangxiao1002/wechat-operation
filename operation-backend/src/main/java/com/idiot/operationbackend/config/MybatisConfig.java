package com.idiot.operationbackend.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.apache.ibatis.reflection.MetaObject;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Instant;
import java.time.LocalDateTime;

/**
 *  mybatis plus config
 * @author wang xiao
 * @date Created in 14:42 2020/10/22
 */
@Configuration
public class MybatisConfig {

     @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
        return interceptor;
    }

    @Bean
    public MetaObjectHandler  metaObjectHandler () {

       return new MetaObjectHandler(){
            @Override
            public void insertFill(MetaObject metaObject) {
                this.setFieldValByName("createTime", Instant.now().toEpochMilli(),metaObject);
            }

            @Override
            public void updateFill(MetaObject metaObject) {
                this.setFieldValByName("updateTime", Instant.now().toEpochMilli(),metaObject);
            }
        };
    }


}
